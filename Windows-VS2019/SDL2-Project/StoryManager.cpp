#include "StoryManager.h"

#include "Game.h"

void StoryManager::startStory() {
	printf("////////////////////////////////\n");
	printf("I finally made it to the light outpost!\n");
	printf("I don't know how long i have left, im the last of my kind\n");
	printf("Fear not world! For the SHADOW HUNTER is here!\n");
	printf("////////////////////////////////\n");
}

void StoryManager::endStory(bool died) {
	if (died == true)
	{
		printf("////////////////////////////////\n");
		printf("And with one final hit, the SHADOW HUNTER was no more\n");
		printf("The shadow creatures corrupted the world...\n");
		printf("Humanity is lost...\n");
		printf("////////////////////////////////\n");
	}
	else if (died == false)
	{
		printf("////////////////////////////////\n");
		printf("TAKE THAT! The SHADOW HUNTER WINS!!\n");
		printf("There is no stopping me now! i must save the whole world!\n");
		printf("The shadow hunter runs off to the next light oupost in hopes of a fresh start...\n");
		printf("////////////////////////////////\n");
	}
	
}

void StoryManager::randomComment() {
	int i = std::rand() % 3;

	if (i == 0)
	{
		printf("////////////////////////////////\n");
		printf("I don't know how long i have left in me,\n");
		printf("I can't give up now! One more push!,\n");
		printf("////////////////////////////////\n");
	}
	if (i == 1)
	{
		printf("////////////////////////////////\n");
		printf("One wave down, one more to go,\n");
		printf("When will this nightmare ever end?\n");
		printf("////////////////////////////////\n");
	}
	if (i == 2)
	{
		printf("////////////////////////////////\n");
		printf("That must be it now surely?\n");
		printf("OH NO! HERE THEY COME!!,\n");
		printf("////////////////////////////////\n");
	}
	if (i == 3)
	{
		printf("////////////////////////////////\n");
		printf("I can do this all day,\n");
		printf("SHOW ME WHAT YOUVE GOT!,\n");
		printf("////////////////////////////////\n");
	}
}