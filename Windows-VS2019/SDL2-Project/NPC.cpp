#include "NPC.h"
#include "Animation.h"
#include "Vector2f.h"
#include "TextureUtils.h"
#include "Game.h"
#include "Player.h"
#include "AABB.h"

#include <stdexcept>
#include <string>

using std::string;

/**
 * NPC
 * 
 * Constructor, setup all the simple stuff. Set pointers to null etc. 
 *  
 */
NPC::NPC() : Sprite()
{
    state = IDLE;
    speed = std::rand() % 100 + 60;
    if (speed != 0)
    {
        npcSavedSpeed = speed;
    }
    maxRange = 0.4f;
    timeToTarget = 0.25f;

    respawnTime = 0;
    currentTime = 0;

    cooldownTimer = 0;
    cooldownMax = std::rand() % 5 + 1;

    targetRectangle.w = SPRITE_WIDTH * 4;
    targetRectangle.h = SPRITE_HEIGHT * 4;
    randomSprite = std::rand() % 6 + 1;
    // Could pass these in to change the difficulty
    // of a NPC for each level etc. 
    maxRange = 500;
    timeToTarget = 1.5f;
    health = 10;
    points = 10;
}

/**
 * init
 * 
 * Function to populate an animation structure from given paramters. 
 *  
 * @param renderer Target SDL_Renderer to use for optimisation.
 * @exception Throws an exception on file not found or out of memory. 
 */
void NPC::init(SDL_Renderer *renderer)
{
    //path string
    string path("assets/maps/Characters.png");

    //postion
    Vector2f position(300.0f,300.0f);

    // Call sprite constructor
    Sprite::init(renderer, path, 6, &position);

    // Setup the animation structure
    animations[LEFT]->init(2, SPRITE_WIDTH, SPRITE_HEIGHT, -1, randomSprite);
    animations[RIGHT]->init(2, SPRITE_WIDTH, SPRITE_HEIGHT, -1, randomSprite);
    animations[UP]->init(2, SPRITE_WIDTH, SPRITE_HEIGHT, -1, randomSprite);
    animations[DOWN]->init(2, SPRITE_WIDTH, SPRITE_HEIGHT, -1, randomSprite);
    animations[IDLE]->init(1, SPRITE_WIDTH, SPRITE_HEIGHT, -1, randomSprite);
    animations[DEAD]->init(1, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 7);

    if (randomSprite == 3)
        speed = 250;

    animations[DEAD]->setLoop(false);

    for (int i = 0; i < maxAnimations; i++)
    {
        animations[i]->setMaxFrameTime(0.2f);
    }

    aabb = new AABB(this->getPosition(), SPRITE_HEIGHT, SPRITE_WIDTH);
}

NPC::~NPC()
{

}

void NPC::update(float dt)
{
    if(state != DEAD) //don't move dead sprite
        ai();
    else    
    {
        velocity->setX(0.0f);
        velocity->setY(0.0f);
    }
    
    Sprite::update(dt);
    cooldownTimer += dt;
}

void NPC::ai()
{
    // get player
    Player* player = game->getPlayer();

    // get distance to player
        // copy the player position
    Vector2f vectorToPlayer(player->getPosition());
        // subtract our position to get a vector to the player
    vectorToPlayer.sub(position);
    float distance = vectorToPlayer.length();

    speed = npcSavedSpeed;
    if (cooldownTimer >= cooldownMax && (randomSprite == 5 || randomSprite == 6))
    {
        fire(velocity);
    }
    // else - head for player

    // Could do with an assign in vector
    velocity->setX(vectorToPlayer.getX());
    velocity->setY(vectorToPlayer.getY());

    // will work but 'wobbles'
    //velocity->scale(speed);   

    // Arrive pattern 
    velocity->scale(timeToTarget);

    if (velocity->length() > speed)
    {
        velocity->normalise();
        velocity->scale(speed);
    }

    state = IDLE;

    if (velocity->getY() > 0.1f)
        state = DOWN;
    else
        if (velocity->getY() < -0.1f)
            state = UP;
    // empty else is not an error!

    if (velocity->getX() > 0.1f)
        state = RIGHT;
    else
        if (velocity->getX() < -0.1f)
            state = LEFT;
    // empty else is not an error. 
}

void NPC::fire(Vector2f* velocity)
{
    game->createBullet(getPosition(), velocity, true);
    cooldownTimer = 0;
}

void NPC::setGame(Game* game)
{
    this->game = game;
}

int NPC::getCurrentAnimationState()
{
    return state;
}

void NPC::takeDamage(int damage)
{
    if (health > 0)
    {
        health -= damage;
        game->getPlayer()->addScore(getPoints());
    }
    else if (health <= 0)
    {
        if (state != DEAD)
        {
            game->amountOfEnemyDeaths++;
            state = DEAD;
        }
    }
}

bool NPC::isDead()
{
    if (health > 0)
        return false;
    else 
        return true;
}

void NPC::respawn(const int MAX_HEIGHT, const int MAX_WIDTH)
{
    health = 10;
    state = IDLE;

    Vector2f randomPostion;

    int doubleWidth = MAX_WIDTH * 2;
    int doubleHeight = MAX_HEIGHT *2;

    // get a random number between 0 and 
    // 2 x screen size. 
    int xCoord = rand()%doubleWidth;
    int yCoord = rand()%doubleHeight;

    // if its on screen move it off. 
    if(xCoord < MAX_WIDTH)
        xCoord *= -1;

    if(yCoord < MAX_HEIGHT)
        yCoord *= -1; 

    randomPostion.setX(xCoord);
    randomPostion.setY(yCoord);

    this->setPosition(&randomPostion);    
}
    
int NPC::getPoints()
{
    return points;
}

void NPC::setPoints(int pointValue)
{
    points = pointValue;
}
    