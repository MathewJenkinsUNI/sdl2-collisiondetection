#include "Player.h"
#include "Animation.h"
#include "Vector2f.h"
#include "TextureUtils.h"
#include "Game.h"
#include "AABB.h"

#include <stdexcept>
#include <string>

using std::string;

// Only types with a fixed bit representaition can be
// defined in the header file
const float Player::COOLDOWN_MAX = 0.3f;

/**
 * Player
 * 
 * Constructor, setup all the simple stuff. Set pointers to null etc. 
 *  
 */
Player::Player() : Sprite()
{
    state = IDLE;
    speed = 150;
    health = 5;

    sprinting = false;

    targetRectangle.w = SPRITE_WIDTH * 4;
    targetRectangle.h = SPRITE_HEIGHT * 4;

    // Initialise weapon cooldown. 
    cooldownTimer = 0;

    // Init points
    points = 0;
    bulletsShot = 0;
}

void Player::init(SDL_Renderer *renderer)
{
    //path string
    string path("assets/maps/Characters.png");

    //postion
    Vector2f position(200.0f,200.0f);

    // Call sprite constructor
    Sprite::init(renderer, path, 5, &position);

    // Setup the animation structure
    animations[LEFT]->init(2, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);
    animations[RIGHT]->init(2, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);
    animations[UP]->init(2, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);
    animations[DOWN]->init(2, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);
    animations[IDLE]->init(1, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);

    for (int i = 0; i < maxAnimations; i++)
    {
        animations[i]->setMaxFrameTime(0.2f);
    }

    aabb = new AABB(this->getPosition(), SPRITE_HEIGHT, SPRITE_WIDTH);

}

Player::~Player()
{

}

void Player::processInput(const Uint8 *keyStates, Vector2f* mousePos)
{
    // Process Player Input

    //Input - keys/joysticks?
    float verticalInput = 0.0f;
    float horizontalInput = 0.0f;

    // If no keys are down player should not animate!
    state = IDLE;

    if (keyStates[SDL_SCANCODE_SPACE] || game->mousePressed == true)
    {
        fire(mousePos);
    }

    // This could be more complex, e.g. increasing the vertical
    // input while the key is held down.
    if (keyStates[SDL_SCANCODE_UP] || keyStates[SDL_SCANCODE_W])
    {
        verticalInput = -1.0f;
        state = UP;
    }

    if (keyStates[SDL_SCANCODE_DOWN] || keyStates[SDL_SCANCODE_S])
    {
        verticalInput = 1.0f;
        state = DOWN;
    }

    if (keyStates[SDL_SCANCODE_RIGHT] || keyStates[SDL_SCANCODE_D])
    {
        horizontalInput = 1.0f;
        state = RIGHT;
    }

    if (keyStates[SDL_SCANCODE_LEFT] || keyStates[SDL_SCANCODE_A])
    {
        horizontalInput = -1.0f;
        state = LEFT;
    }

    // Calculate player velocity.
    velocity->setX(horizontalInput);
    velocity->setY(verticalInput);
    velocity->normalise();
    velocity->scale(speed);
}

void Player::changeSpeed(bool pressed)
{
    if (pressed)
    {
        speed = 300;
        sprinting = true;
    }
    else if (!pressed)
    {
        speed = 150;
        sprinting = false;
    }
}

int Player::getCurrentAnimationState()
{
    return state;
}

void Player::setGame(Game* game)
{
    this->game = game;
}

void Player::update(float timeDeltaInSeconds)
{
    Sprite::update(timeDeltaInSeconds);

    cooldownTimer += timeDeltaInSeconds;
}

void Player::fire(Vector2f* velocity)
{   
    if(cooldownTimer > COOLDOWN_MAX && !sprinting)
    {
        bulletsShot++;

        game->createBullet(position, velocity, false);
        cooldownTimer = 0;
    }   
}

void Player::takeDamage(int damage)
{
    health -= damage;
    printf("\nTAKEN DAMAGE (1)\n");
    printf("health = %d \n", health);
}

void Player::addScore(int points)
{
    this->points += points;
}

int Player::getScore()
{
    return points;
}